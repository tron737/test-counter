<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 01.08.2018
 * Time: 16:38
 */

namespace Controllers;


use App\Controller;
use Models\Files;
use Models\WordProcessing;

class IndexController extends Controller
{
    public function index()
    {
        if ($_FILES) {
            $files = new Files();

            $resultUpload = $files->upload($_FILES);

            if ($resultUpload) {
                $fileData = $files->read($resultUpload);

                $wordProcess = new WordProcessing();
                $fileData = $wordProcess->treatment($fileData);
                $fileData = $wordProcess->counter($fileData);

                $this->result['result'] = $fileData;
            } else {
                $this->result['error'] = 'error';
            }
        }
        $this->view('index');
    }
}