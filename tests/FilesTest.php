<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 01.08.2018
 * Time: 23:02
 */

namespace tests;


use Models\Files;
use PHPUnit\Framework\TestCase;

class FilesTest extends TestCase
{
    public function testUpload()
    {
        $files = new Files();
        $this->assertFalse($files->upload([]));
    }

    public function testRead()
    {
        $files = new Files();
        $this->assertEquals('', $files->read(''));
    }
}