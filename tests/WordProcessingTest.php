<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 02.08.2018
 * Time: 0:28
 */

namespace tests;

use Models\WordProcessing;
use PHPUnit\Framework\TestCase;

class WordProcessingTest extends TestCase
{
    public function treatmentTest()
    {
        $word = new WordProcessing();

        $this->assertEquals('', $word->treatment(''));
    }

    public function treatmentSymbolTest()
    {
        $word = new WordProcessing();

        $this->assertEquals('', $word->treatment('.,'));
    }

    public function treatmentStringTest()
    {
        $word = new WordProcessing();

        $this->assertEquals('test', $word->treatment('test,.'));
    }

    public function counterTest()
    {
        $word = new WordProcessing();

        $this->assertEquals([], $word->counter(''));
    }
}