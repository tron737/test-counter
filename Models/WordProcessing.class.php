<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 01.08.2018
 * Time: 19:30
 */

namespace Models;


class WordProcessing
{
    /**
     * Подсчет слов в тексте
     * @param string $text
     * @return array
     */
    public function counter($text = '')
    {
        $textArr = explode(' ', $text);

        $wordCounts = [];

        foreach ($textArr as $word) {
            if ($word) {
                if (!$wordCounts[$word]) {
                    $wordCounts[$word] = 0;
                }
                ++$wordCounts[$word];
            }
        }

        return $wordCounts;
    }

    /**
     * Обработка текста, удаление символов
     * @param string $text входной текст
     * @return mixed
     */
    public function treatment($text)
    {
        $symbols = [
            '.',
            ',',
            ':',
            ';',
            '-',
            '–',
            "\n",
            "\r",
            "\r\n",
            "\n\r",
            '«',
            '»'
        ];
        $text = str_replace($symbols, ' ', $text);

        return $text;
    }
}