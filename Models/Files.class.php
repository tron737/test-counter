<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 01.08.2018
 * Time: 19:09
 */

namespace Models;


class Files
{
    /**
     * Сохранение файла в /upload/
     * @param array $file суперглобальный массив $_FILES
     * @return bool|string
     */
    public function upload($file)
    {
        if (!$file || empty($file)) {
            return false;
        }
        $uploadFile = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . time() . '_' . basename($_FILES['userfile']['name']);
        $resultUpload = move_uploaded_file($file['userfile']['tmp_name'], $uploadFile);

        return $resultUpload ? $uploadFile : $resultUpload;
    }

    /**
     * Чтение файла
     * @param string $filePath путь к файлу
     * @return bool|string
     */
    public function read($filePath)
    {
        if (!$filePath) {
            return false;
        }
        $result = file_get_contents($filePath);

        return $result;
    }
}