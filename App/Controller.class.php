<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 31.07.2018
 * Time: 20:15
 */

namespace App;


class Controller
{
    public $controllerDirectory = 'Controllers';
    public $templateDirectory = 'Template';
    /**
     * @var string $mainTemplate шаюлон по умолчанию
     */
    public $mainController = 'index';

    public $templateFile;

    public $controllerName;

    public $controllerPath;

    public $controller;

    public $result = null;

    public function runController($path)
    {
        if ($path[0]) {
            $templateName = $path[0];
        } else {
            $templateName = $this->mainController;
        }

        $this->controllerName = ucfirst($templateName) . 'Controller';

        $this->controllerPath = $this->controllerDirectory . '\\' . $this->controllerName;

        if (!class_exists($this->controllerPath)) {
            $this->view('404');
            return false;
        }

        $this->controller = new $this->controllerPath();

        if (!method_exists($this->controller, $templateName)) {
            $this->view('404');
            return false;
        }

        $this->controller->$templateName();

        return true;
    }

    public function view($templateName = '')
    {
        if (!$templateName) {
            $templateName = '404';
        }

        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->templateDirectory . '/' . $templateName . '.php';

        if (!file_exists($filePath)) {
            echo 'not found template';
            return false;
        }

        require_once $_SERVER['DOCUMENT_ROOT'] . '/' . $this->templateDirectory . '/' . $templateName . '.php';
        return true;
    }
}