<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 31.07.2018
 * Time: 20:14
 */

namespace App;


class App extends Controller
{
    public function __construct($path)
    {
        $name = explode('/', $path);
        $args = array();

        foreach ($name as $item) {
            if(!empty($item) && $item != $name[0]) {
                $args[] = $item;
            }
        }

        $this->runController(explode('/', $path), $args);
    }

}