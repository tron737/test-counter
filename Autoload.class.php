<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 31.07.2018
 * Time: 20:09
 */

class Autoload
{
    public static function Load($className)
    {
        $fileName = $_SERVER['DOCUMENT_ROOT'] . '/' . str_replace("\\", '/', $className) . '.class.php';

        if (file_exists($fileName)) {
            require_once $fileName;
            if (class_exists($className)) {
                return true;
            }
        }
        return false;
    }
}

spl_autoload_register('Autoload::Load');